/**
 * Created by daniel on 2016-01-27.
 */
var f = require("faker");
var authors = require('./authors.js').authors;
var _ = require("lodash");

module.exports = {
  topics: [
    {
      id: 1,
      title: f.commerce.productName(),
      created: f.date.past(),
      author: {
        id: 1,
        username: _.find(authors, {id: 1}).username
      }
    },
    {
      id: 2,
      title: f.commerce.productName(),
      created: f.date.past(),
      author: {
        id: 2,
        username: _.find(authors, {id: 2}).username
      }
    },
    {
      id: 3,
      title: f.commerce.productName(),
      created: f.date.past(),
      author: {
        id: 3,
        username: _.find(authors, {id: 3}).username
      }
    },
    {
      id: 4,
      title: f.commerce.productName(),
      created: f.date.past(),
      author: {
        id: 4,
        username: _.find(authors, {id: 4}).username
      }
    },
    {
      id: 5,
      title: f.commerce.productName(),
      created: f.date.past(),
      author: {
        id: 5,
        username: _.find(authors, {id: 5}).username
      }
    }
  ]
};
