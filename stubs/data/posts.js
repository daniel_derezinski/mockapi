/**
 * Created by daniel on 2016-01-28.
 */
var f = require("faker");
var authors = require('./authors.js').authors;
var _ = require("lodash");

module.exports = {
  posts: [
    {
      id: 1,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 1,
        username: _.find(authors, {id: 1}).username
      },
      topic: {
        id: 1
      }
    },
    {
      id: 2,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 2,
        username: _.find(authors, {id: 2}).username
      },
      topic: {
        id: 1
      }
    },
    {
      id: 3,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 5,
        username: _.find(authors, {id: 5}).username
      },
      topic: {
        id: 1
      }
    },
    {
      id: 4,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 2,
        username: _.find(authors, {id: 2}).username
      },
      topic: {
        id: 2
      }
    },
    {
      id: 5,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 3,
        username: _.find(authors, {id: 3}).username
      },
      topic: {
        id: 2
      }
    },
    {
      id: 6,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 2,
        username: _.find(authors, {id: 2}).username
      },
      topic: {
        id: 2
      }
    },
    {
      id: 7,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 5,
        username: _.find(authors, {id: 5}).username
      },
      topic: {
        id: 3
      }
    },
    {
      id: 8,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 4,
        username: _.find(authors, {id: 4}).username
      },
      topic: {
        id: 3
      }
    },
    {
      id: 9,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 4,
        username: _.find(authors, {id: 4}).username
      },
      topic: {
        id: 3
      }
    },
    {
      id: 10,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 2,
        username: _.find(authors, {id: 2}).username
      },
      topic: {
        id: 4
      }
    },
    {
      id: 11,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 1,
        username: _.find(authors, {id: 1}).username
      },
      topic: {
        id: 4
      }
    },
    {
      id: 12,
      content: f.lorem.paragraphs(),
      created: f.date.past(),
      author: {
        id: 5,
        username: _.find(authors, {id: 5}).username
      },
      topic: {
        id: 4
      }
    }
  ]
};
