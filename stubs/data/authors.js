/**
 * Created by daniel on 2016-01-27.
 */
var f = require('faker');

module.exports = {
  authors : [
    {
      id: 1,
      username: f.internet.userName(),
      email: f.internet.email(),
      joined: f.date.past()
    },
    {
      id: 2,
      username: f.internet.userName(),
      email: f.internet.email(),
      joined: f.date.past()
    },
    {
      id: 3,
      username: f.internet.userName(),
      email: f.internet.email(),
      joined: f.date.past()
    },
    {
      id: 4,
      username: f.internet.userName(),
      email: f.internet.email(),
      joined: f.date.past()
    },
    {
      id: 5,
      username: f.internet.userName(),
      email: f.internet.email(),
      joined: f.date.past()
    }
  ]
};
