/**
 * Created by daniel on 02.03.16.
 */
module.exports = {
  users: [
    {
      "name": "Adam Nowak",
      "position": "WebDeveloper",
      "avatar": "img/1.png"
    },
    {
      "name": "Grzegorz Kowalski",
      "position": "Front-end Developer",
      "avatar": "img/2.png"
    },
    {
      "name": "Rafał Nijaki",
      "position": "Back-end Developer",
      "avatar": "img/3.png"
    }
  ],
  team1: [
    {
      "name": "Adam Nowak",
      "position": "WebDeveloper",
      "avatar": "img/1.png",
      "email": "adam@britenet.com.pl"
    },
    {
      "name": "Grzegorz Kowalski",
      "position": "Front-end Developer",
      "avatar": "img/2.png",
      "email": "grzegorz@britenet.com.pl"
    },
    {
      "name": "Rafał Nijaki",
      "position": "Back-end Developer",
      "avatar": "img/3.png",
      "email": "rafal@britenet.com.pl"
    }
  ],
  team2: [
    {
      "name": "Michał Kowal",
      "position": "WebDeveloper",
      "avatar": "img/4.png",
      "email": "michal@britenet.com.pl"
    },
    {
      "name": "Arek Suchy",
      "position": "Front-end Developer",
      "avatar": "img/5.png",
      "email": "arek@britenet.com.pl"
    },
    {
      "name": "Rafał Nijaki",
      "position": "Back-end Developer",
      "avatar": "img/6.png",
      "email": "rafal@britenet.com.pl"
    }
  ],
  leader: {
    "name": "Marek Pierwszy",
    "position": "Leader",
    "avatar": "img/7.png"
  }
};
