/**
 * Created by daniel on 2016-01-28.
 */
var posts = require("../data/posts.js");
var _ = require("lodash");


var generateResponse = function (req, res, next) {
  //res.status(400).send('Bad requests');

  console.log(req.body);

  var postsList = posts.posts;

  var lastPost = _.last(postsList);

  var newPost = req.body;
  newPost.id = lastPost.id + 1;
  postsList.push(newPost);

  if (newPost) {
    res.status(201)
    res.body = {id: newPost.id}
  } else {
    res.status(400)
  }
  next();
};

module.exports = {
  path: '/posts',
  method: "POST",
  callback: generateResponse
};
