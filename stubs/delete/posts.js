/**
 * Created by daniel on 2016-01-28.
 */
var posts = require("../data/posts.js");
var _ = require("lodash");


var generateResponse = function (req, res, next) {
  //res.status(400).send('Bad requests');
  console.log(req.params.id);
  if (req.params.id) {
    var id = parseInt(req.params.id);
    var postCount = posts.posts.length;
    _.remove(posts.posts, function (ele) {
      return ele.id === id;
    });
    var deleteCount = postCount - posts.posts.length;
    if (deleteCount > 0 ) {
      res.status(202);
      res.body = {count: deleteCount};
    } else {
      res.status(400)
    }
  } else {
    res.status(400);
  }
  next();
};

module.exports = {
  path: '/posts/:id',
  method: "DELETE",
  callback: generateResponse
};
