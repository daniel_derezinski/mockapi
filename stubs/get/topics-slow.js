/**
 * Created by daniel on 2016-01-27.
 */
var topics = require("../data/topics.js");

module.exports = {
  path: '/slow/topics',
  delay: 3000,
  method: "GET",
  template: topics.topics
}

