/**
 * Created by daniel on 2016-01-28.
 */
var posts = require("../data/posts.js");
var _ = require("lodash");


var generateResponse = function (req, res, next) {
  //res.status(400).send('Bad requests');
  var postsList = posts.posts;
  var postsForTopic = _.filter(postsList, {topic: {id: parseInt(req.params.id)}});

  if (postsForTopic) {
    res.body = postsForTopic;
    res.status(200)
  } else {
    res.status(400)
  }
  next();
};

module.exports = {
  path: '/posts/topic/:id',
  method: "GET",
  callback: generateResponse
};
