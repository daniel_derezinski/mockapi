/**
 * Created by daniel on 2016-01-28.
 */
var authors = require("../data/authors.js");

module.exports = {
  path: '/authors',
  method: "GET",
  template: authors.authors
};
