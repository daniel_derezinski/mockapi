/**
 * Created by daniel on 02.03.16.
 */
var users = require("../data/users.js");


var generateResponse = function (req, res, next) {
  //res.status(400).send('Bad requests');
  var id = req.params.id;
  var result;
  if (!id) {
    result = users.users;
  } else {
    result = users[id]
  }

  if (result) {
    res.body = result;
    res.status(200)
  } else {
    res.status(400)
  }
  next();
};

module.exports = {
  path: '/users',
  method: "GET",
  callback: generateResponse
};
