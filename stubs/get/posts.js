/**
 * Created by daniel on 02.03.16.
 */
var posts = require("../data/posts.js");
var _ = require("lodash");


var generateResponse = function (req, res, next) {
  //res.status(400).send('Bad requests');
  var postsList = posts.posts;
  //var postsForTopic = _.filter(postsList, {topic: {id: parseInt(req.params.id)}});

  if (postsList) {
    res.body = postsList;
    res.status(200)
  } else {
    res.status(400)
  }
  next();
};

module.exports = {
  path: '/posts',
  method: "GET",
  callback: generateResponse
};
