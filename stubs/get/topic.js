/**
 * Created by daniel on 2016-01-27.
 */
var topics = require("../data/topics.js");
var _ = require("lodash");

var generateResponse = function (req, res, next) {
  //res.status(400).send('Bad requests');
  var topicsList = topics.topics;
  var topic = _.find(topicsList, {id: parseInt(req.params.id)});

  if (topic) {
    res.body = topic;
    res.status(200)
  } else {
    res.status(400)
  }
  next();
};

module.exports = {
  path: '/topics/:id',
  method: "GET",
  callback: generateResponse
};
