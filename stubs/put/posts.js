/**
 * Created by daniel on 2016-01-28.
 */
var posts = require("../data/posts.js");
var _ = require("lodash");


var generateResponse = function (req, res, next) {
  //res.status(400).send('Bad requests');

  var postIdx = _.findIndex(posts.posts, {id: parseInt(req.params.id)});

  if (postIdx > -1) {
    posts.posts[postIdx] = req.body;
    res.status(202);
    res.body = {count: 1}
  } else {
    res.status(400)
  }
  next();
};

module.exports = {
  path: '/posts/:id',
  method: "PUT",
  callback: generateResponse
};
